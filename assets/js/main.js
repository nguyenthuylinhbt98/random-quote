    const getQuote = () => {
        $.ajax(' http://quotes.stormconsultancy.co.uk/random.json').then(function success(res){
            $("#quote").html(
                `
                <div class="content">
                    <p>${res.quote}</p>
                </div>
                <div class=" author">
                    <p>&#8764; <i>${res.author}</i> &#8764;</p>
                </div>
                <div class="share">
                    <button class="btn btn-primary"><i class="fab fa-facebook-f"></i></button>
                </div>
                `
            )
        })
    }

    $('#new-quote').on('click', getQuote)

    getQuote();

    

